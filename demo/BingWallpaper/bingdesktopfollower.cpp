#include "bingdesktopfollower.h"
#include "picturehistory.h"
#include <QtGui>
#include <QtWebKit/QtWebKit>
#include <QtNetwork>
#include <QDesktopWidget>
#include <QDateTime>
#include <windows.h>
#include <sstream>

#include <QDebug>

/**
  Public:
*/
/* constructor of BingDesktopFollower */
BingDesktopFollower::BingDesktopFollower(QWidget *parent) :
    QWidget(parent)
{
    qDebug() << "BingDesktopFollower ctor ...";
    initData();
    initGUI();
    initContextMenu();
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowFlags(Qt::CustomizeWindowHint);
    fetch();
}

/* destructor of BingDesktopFollower */
BingDesktopFollower::~BingDesktopFollower()
{
    qDebug() << "BingDesktopFollower dtor ...";
    delete bing_;
    delete history_;
    delete currentPicture_;
    delete dragPos_;
}


/**
  Public Slots:
*/
/* try to fetch the content of a specified web page */
void BingDesktopFollower::fetch()
{
    qDebug() << "fetching ...";
    bing_->load(QUrl("http://cn.bing.com"));
}

/* show the previous picture in widget */
void BingDesktopFollower::prev()
{
    qDebug() << "previous picture";
    QString prev = history_->prev();
    if(!prev.isEmpty())
    {
        currentFileName_ = prev;
        currentPicture_->load(currentFileName_);
        repaint();
    }
}

/* show the next picture in widget */
void BingDesktopFollower::next()
{
    qDebug() << "next picture";
    QString next = history_->next();
    if(!next.isEmpty())
    {
        currentFileName_ = next;
        currentPicture_->load(currentFileName_);
        repaint();
    }
}

/* set the current picture as desktop wallpaper */
void BingDesktopFollower::setDesktopWallpaper()
{
    qDebug() << "set desktop wallpaper ...";

    const char *tmp = QDir::currentPath().append("/").append(currentFileName_).toStdString().c_str();
    std::wstringstream wss;
    wss << tmp;
    const wchar_t *filename = wss.str().c_str();
//    qDebug() << *filename;
    if(SystemParametersInfo(SPI_SETDESKWALLPAPER, 0, (void*)filename, SPIF_UPDATEINIFILE) == FALSE)
    {
        emit message(FAILED_SET_WALLPAPER);
    }
}

/**
  Private Slots:
*/
/* process the fetched content */
void BingDesktopFollower::onload(bool success)
{
    qDebug() << "onload: success = " << success;
    if(success)
    {
        QRegExp rxp("background-image\\s*:\\s*url\\s*[(](.+)[)]", Qt::CaseInsensitive);
        int pos = 0;
        rxp.setMinimal(true);
//        bing_->show();
        QWebElement bgDiv = bing_->page()->currentFrame()->findFirstElement("div#bgDiv");
        QString style = bgDiv.attribute("style");
        if(style.contains(rxp))
        {
            pos = rxp.indexIn(style);
            qDebug() << "extracted image path: " << rxp.cap(1);
            downloadPicture(QUrl(rxp.cap(1)));
        }
        if(-1 == pos)
        {
            emit message(RESOURCE_LOCATE_ERROR);
        }
    }
    else
    {
        if(retry_counter_ >= 3)
        {
            emit message(PAGELOAD_ERROR);
            retry_counter_ = 0;
            return;
        }
        fetch();
        retry_counter_++;
    }
}

/* get the picture from http://cn.bing.com */
void BingDesktopFollower::replyFinished(QNetworkReply *reply)
{
    qDebug() << "reply finished ...";
    if(reply->error() == QNetworkReply::NoError)
    {
        currentPicture_->loadFromData(reply->readAll());
        QDateTime now;
        QString filename = now.currentDateTime().toString("yyMMddhhmmss.jpg");
        currentPicture_->save(filename);
        qDebug() << "picture saved as " << filename;
        currentFileName_ = filename;
        history_->addPicture(reply->url(), filename);
        repaint();
    }
    else
    {
        qDebug() << reply->errorString();
        emit message(DOWNLOAD_ERROR);
    }
}

/* show current status of the application */
void BingDesktopFollower::showMessage(MESSAGETYPE type)
{
    QString msg;
    switch(type)
    {
    case PAGELOAD_ERROR:
        msg = "pageload error ...";
        break;
    case RESOURCE_LOCATE_ERROR:
        msg = "resource locate error ...";
        break;
    case DOWNLOAD_ERROR:
        msg = "download error ...";
        break;
    case NOUPDATE:
        msg = "no new picture avaliable ...";
        break;
    case FAILED_SET_WALLPAPER:
        msg = "failed to set the picture as wallpaper ...";
        break;
    default:
        break;
    }
    qDebug() << "MessageFrom(showMessage slot): " << type << " " << msg;
}

/**
  Protected:
*/
void BingDesktopFollower::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    if(!currentPicture_->isNull())
    {
        QPixmap scaled = currentPicture_->scaled(w_, h_);
        painter.drawPixmap(0, 0, w_, h_, scaled);
    }
    else
    {
        painter.setBrush(Qt::gray);
        painter.drawRect(0, 0, w_, h_);
    }
    QWidget::paintEvent(event);
}

void BingDesktopFollower::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        *dragPos_ = event->globalPos() - frameGeometry().topLeft();
        event->accept();
    }
}

void BingDesktopFollower::mouseMoveEvent(QMouseEvent *event)
{
    if(event->buttons() & Qt::LeftButton)
    {
        move(event->globalPos() - *dragPos_);
        event->accept();
    }
}

/**
  Private:
*/
/* initialize data */
void BingDesktopFollower::initData()
{
    qDebug() << "init data ...";
    bing_ = new QWebView;
    nam_ = new QNetworkAccessManager(this);
    history_ = new PictureHistory;
    currentPicture_ = new QPixmap;
    currentFileName_ = "";
    dragPos_ = new QPoint;
    aspectRatio_ = 0.20;
    retry_counter_ = 0;
    retry_max_times_ = 3;

    connect(bing_, SIGNAL(loadFinished(bool)), this, SLOT(onload(bool)));
    connect(nam_, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));
    connect(this, SIGNAL(message(MESSAGETYPE)), this, SLOT(showMessage(MESSAGETYPE)));
}

/* initialize user interface */
void BingDesktopFollower::initGUI()
{
    qDebug() << "init GUI ...";
    QDesktopWidget widget;
    w_ = widget.width() * aspectRatio_;
    h_ = widget.height() * aspectRatio_;
    resize(w_, h_);
    move(widget.width() * (1 - aspectRatio_) - 20, 20);
    setMinimumSize(w_, h_);
    setMaximumSize(w_, h_);
}

/* initialize context menu */
void BingDesktopFollower::initContextMenu()
{
    qDebug() << "init context menu ...";
    QAction *act_update = new QAction(QIcon(":/icons/update.png"), tr("&Update"), this);
    connect(act_update, SIGNAL(triggered()), this, SLOT(fetch()));

    QAction *act_prev = new QAction(QIcon(":/icons/prev.png"), tr("&Prev"), this);
    connect(act_prev, SIGNAL(triggered()), this, SLOT(prev()));

    QAction *act_next = new QAction(QIcon(":/icons/next.png"), tr("&Next"), this);
    connect(act_next, SIGNAL(triggered()), this, SLOT(next()));

    QAction *act_set_wallpaper = new QAction(QIcon(":/icons/wallpaper.png"), tr("&Set As Wallpaper"), this);
    connect(act_set_wallpaper, SIGNAL(triggered()), this, SLOT(setDesktopWallpaper()));

    QAction *act_exit = new QAction(QIcon(":/icons/exit.png"), tr("E&xit"), this);
    connect(act_exit, SIGNAL(triggered()), this, SLOT(deleteLater()));

    QAction *separator = new QAction(this);
    separator->setSeparator(true);

    addAction(act_update);
    addAction(act_prev);
    addAction(act_next);
    addAction(act_set_wallpaper);
    addAction(separator);
    addAction(act_exit);

    setContextMenuPolicy(Qt::ActionsContextMenu);
}

/* download the picture if it does not exist at the local */
void BingDesktopFollower::downloadPicture(QUrl url)
{
    qDebug() << "download picture ...";
    if(history_->exist(url))
    {
        emit message(NOUPDATE);
        currentFileName_ = history_->get(url);
        currentPicture_->load(currentFileName_);
        repaint();
    }
    else
    {
        qDebug() << url;
        nam_->get(QNetworkRequest(url));
    }
}

/* save the picture to a specified place */
void BingDesktopFollower::savePicture(const QString &filename) const
{

}



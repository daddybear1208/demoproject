#ifndef BINGDESKTOPFOLLOWER_H
#define BINGDESKTOPFOLLOWER_H

#include <QWidget>
class QUrl;
class QWebView;
class QNetworkAccessManager;
class QNetworkReply;
class PictureHistory;


class BingDesktopFollower : public QWidget
{
    Q_OBJECT
public:
    enum MESSAGETYPE
    {
        PAGELOAD_ERROR, RESOURCE_LOCATE_ERROR, DOWNLOAD_ERROR, NOUPDATE, FAILED_SET_WALLPAPER
    };
    explicit BingDesktopFollower(QWidget *parent = 0);
    virtual ~BingDesktopFollower();

public slots:
    void fetch();
    void prev();
    void next();
    void setDesktopWallpaper();

private slots:
    void onload(bool);
    void replyFinished(QNetworkReply*);
    void showMessage(MESSAGETYPE);

signals:
    void message(MESSAGETYPE) const;

protected:
    void paintEvent(QPaintEvent *);
    void mousePressEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);

private:
    void initData();
    void initGUI();
    void initContextMenu();
    void downloadPicture(QUrl);
    void savePicture(const QString&) const;

    QWebView *bing_;
    QNetworkAccessManager *nam_;
    PictureHistory *history_;
    QPixmap *currentPicture_;
    QString currentFileName_;
    QPoint *dragPos_;
    float aspectRatio_;
    float w_, h_;
    int retry_counter_;
    int retry_max_times_;
};

#endif // BINGDESKTOPFOLLOWER_H

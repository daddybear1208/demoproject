#include "picturehistory.h"
#include <QFile>
#include <QDataStream>
#include <QMessageBox>
#include <QDebug>

PictureHistory::PictureHistory()
    :FILENAME("hist.dat")
{
    qDebug() << "PictureHistory::PictureHistory()";
    history_ = NULL;
    load();
    current_ = history_->count() - 1;
}

bool PictureHistory::exist(QUrl url) const
{
    return history_->contains(PicInfo(url, ""));
}

void PictureHistory::addPicture(const QUrl &url, const QString &filename)
{
    history_->append(PicInfo(url, filename));
    current_ = history_->count() - 1;
    save();
}

void PictureHistory::delPicture(const QUrl &url)
{
    int del = history_->indexOf(PicInfo(url, ""));
    history_->removeAt(del);
    if(del == current_)
    {
        current_ = history_->count() - 1;
    }
    save();
}

void PictureHistory::load()
{
    qDebug() << "PictureHistory::load()";
    QFile file(FILENAME);
    if(!file.exists())
    {
        file.open(QFile::WriteOnly);
        file.close();
    }
    if(file.open(QFile::ReadOnly))
    {
        QDataStream in(&file);
        if(history_ == NULL)
            history_ = new QList<PicInfo>();
        else
            history_->clear();
        in >> *history_;
        file.close();
    }
    else
    {
        QMessageBox::warning(0,
                             "Error",
                             "Unable to load file!",
                             QMessageBox::Ok);
    }
}

void PictureHistory::save() const
{
    qDebug() << "PictureHistory::save()";
    QFile file(FILENAME);
    if(file.open(QFile::WriteOnly))
    {
        QDataStream out(&file);
        out << *history_;
        file.close();
    }
    else
    {
        QMessageBox::warning(0,
                             "Error",
                             "Unable to save file!",
                             QMessageBox::Ok);
    }
}

QString PictureHistory::get(QUrl url) const
{
    return history_->at(history_->indexOf(PicInfo(url, ""))).FileName;
}

QString PictureHistory::prev()
{
    if(history_->count() == 0)
        return QString();
    current_--;
    if(current_ < 0)
        current_ = history_->count() - 1;
    return history_->at(current_).FileName;
}

QString PictureHistory::next()
{
    if(history_->count() == 0)
        return QString();
    current_++;
    if(current_ >= history_->count())
        current_ = 0;
    return history_->at(current_).FileName;
}

QString PictureHistory::rand()
{
    current_ = qrand() % history_->count();
    return history_->at(current_).FileName;
}

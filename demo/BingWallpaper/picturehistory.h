#ifndef PICTUREHISTORY_H
#define PICTUREHISTORY_H

#include <QUrl>
#include <QString>
#include "picinfo.h"

class PictureHistory
{
public:
    PictureHistory();
    bool exist(QUrl) const;
    void addPicture(const QUrl &, const QString &);
    void delPicture(const QUrl &);
    void load();
    void save() const;
    QString get(QUrl) const;
    QString prev();
    QString next();
    QString rand();

private:
    const QString FILENAME;
    QList<PicInfo> *history_;
    int current_;
};

#endif // PICTUREHISTORY_H

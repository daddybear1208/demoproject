//
//  main.cpp
//  DataStructure
//
//  Created by GEEK on 3/23/15.
//  Copyright (c) 2015 GEEK. All rights reserved.
//

#include <stdio.h>
#ifdef WIN32
#include <windows.h>
#endif

typedef struct Node
{
	int data;
	struct Node* next;

}Node;


void ConstructLinkedList(int* array, int num, Node* header)
{
	Node* currNode = header;
	Node* node;
	for (int i = 0; i < num; i++)
	{
		node = new Node;
		node->data = array[i];
		currNode->next = node;
		currNode = node;
	}
	currNode->next = 0;
}

void PrintLinkedList(Node* header)
{
	Node* currNode = header->next;
	while (currNode) {
		printf("%d ", currNode->data);
		currNode = currNode->next;
	}
}

void InsertSortLinkedList(Node* header)
{
	Node* unsortedNode = header->next;
	if (!unsortedNode)
		return;

	Node* preUnsortedNode = unsortedNode;
	unsortedNode = unsortedNode->next;

	int sortingProcess = 0;

	while (unsortedNode)
	{
		Node* preNode = header;

		while (preNode->next != unsortedNode)
		{
			if (preNode->next->data < unsortedNode->data)
			{
				preNode = preNode->next;
			}
			else
			{
				// insert unsorted node after preNode
				preUnsortedNode->next = unsortedNode->next;

				unsortedNode->next = preNode->next;
				preNode->next = unsortedNode;
				unsortedNode = preUnsortedNode->next;
				break;
			}
		}

		// if the unsorted node doesn't need to be moved
		// during the sorting process, the unsortedNode and
		// preUnsortedNode both need to be moved one step
		// behind manually
		if (preNode->next == unsortedNode)
		{
			unsortedNode = unsortedNode->next;
			preUnsortedNode = preUnsortedNode->next;
		}

		printf("sorting process %d: ", sortingProcess);
		PrintLinkedList(header);
		sortingProcess++;
		printf("\n");
	}
}


int main(int argc, char* argv[])
{
	printf("please input integer number: ");
	int numCount = 0;

	scanf("%d", &numCount);

	printf("please input %d numbers: ", numCount);

	int* numArray = new int[numCount];

	for (int i = 0; i < numCount; i++)
	{
		scanf("%d", numArray + i);
	}

	Node* header = new Node();
	header->data = 0;
	header->next = 0;

	ConstructLinkedList(numArray, numCount, header);

	printf("original linked list: ");
	PrintLinkedList(header);
	printf("\n");

	InsertSortLinkedList(header);
	printf("sorted linked list: ");

	PrintLinkedList(header);
	printf("\n");


	// memory recycle
	delete[] numArray;
	while (header) {
		Node* node = header;
		header = header->next;
		delete node;
	}

#ifdef WIN32
	system("pause");
#endif
	return 0;
}
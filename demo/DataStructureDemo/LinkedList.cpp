#include "LinkedList.h"

Node::Node() 
:m_coef(0), m_exp(0)
{

}

Node::Node(int coef, int exp)
:m_coef(coef), m_exp(exp)
{
}


int Node::coef()
{
    return m_coef;
}

int Node::exp()
{
	return m_exp;
}

Node* Node::next()
{
    return m_next;
}


void Node::setNext(Node *next)
{
    m_next = next;
}

void Node::setCoef(int coef)
{
	m_coef = coef;
}

void Node::setExp(int exp)
{
	m_exp = exp;
}

LinkedList::LinkedList()
{
    m_head = new Node;
	m_head->setCoef(0);
	m_head->setExp(0);
    m_head->setNext(0);
}

LinkedList::~LinkedList()
{
    clear();
    delete m_head;
}

void LinkedList::clear()
{
    if (m_head->next()==0)
    {
        return;
    }
    
    Node* node = m_head->next();
    while (node != 0)
    {
        Node* node0 = node->next();
        delete node;
        node = node0;
    }
    
    m_head->setNext(0);
}

bool LinkedList::isEmpty() const
{
    return (m_head->next() == 0);
}

int LinkedList::length() const
{
    int length = 0;
    Node* head = m_head;
    while (head->next() != 0)
    {
        length++;
        head = head->next();
    }
    
    return length;
}

Node* LinkedList::nodeAt(int index) const
{
    Node* node = 0;
    if (index >= length())
    {
        return 0;
    }
    
    node = m_head;
    for (int i = 0; i < index + 1; i++)
    {
        node = node->next();
    }
    return node;
}

bool LinkedList::insert(int index, Node* node)
{
    if (index >= length())
    {
        return false;
    }
    
    if(index < 0)
    {
        node->setNext(m_head->next());
        m_head->setNext(node);
        return true;
    }
    
    Node* node0 = nodeAt(index);
    node->setNext(node0->next());
    node0->setNext(node);
    
    return true;
    
}

void LinkedList::append(Node* node)
{
    int index = length()-1;
    if (length() == 0)
    {
        m_head->setNext(node);
        node->setNext(0);
    }
    else
    {
        insert(index, node);
    }
}

bool LinkedList::removeAt(int index)
{
    int len = length();
    if (index >= len)
    {
        return false;
    }
    
    if (len == 1)
    {
        delete m_head->next();
        m_head->setNext(0);
        return true;
    }
    
    Node* node = nodeAt(index - 1);
    Node* node0 = node->next()->next();
    delete node->next();
    node->setNext(node0);
    
    
    return true;
}

void LinkedList::prepend(Node *node)
{
    insert(-1, node);
}

LinkedList* LinkedList::clone() const
{
	LinkedList* list = new LinkedList;
	// insert every node into the new list
	Node* pNode = m_head;
	while (pNode->next())
	{
		pNode = pNode->next();
		Node* node = new Node(pNode->coef(), pNode->exp());
		list->append(node);
	}

	return list;


}










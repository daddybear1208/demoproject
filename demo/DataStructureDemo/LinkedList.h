#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <iostream>
using namespace std;

class LinkedList;
class Number;

class Node
{
public:
	friend class LinkedList;
	friend class Number;

    Node(int coef,int exp);
    
    Node();
    
    int coef();

	int exp();
    
    Node* next();
    
    void setCoef(int coef);

	void setExp(int exp);
    
    void setNext(Node* next);
    
private:
    int m_coef;
	int m_exp;
    Node* m_next;
};

class LinkedList
{
public:
    LinkedList();
    
    ~LinkedList();
    
    void clear();

	LinkedList* clone() const;
    
    bool isEmpty() const;
    
    int length() const;
    
    Node* nodeAt(int index) const;
    
    bool insert(int index, Node* node);
    
    void append(Node* node);
    
    void prepend(Node* node);
    
    bool removeAt(int index);
    
private:
    Node* m_head;
};




#endif /* defined(LINKEDLIST_H) */

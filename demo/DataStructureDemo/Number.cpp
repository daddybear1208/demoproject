#include "Number.h"

Number::Number(int data[], int n, SIGN sign )
:m_sign(sign)
{
	m_data = new LinkedList;
	if (!n)
		return;
	Node* node = 0;
	for (int i = 0; i < n; i++)
	{
		if (!data[i])
			continue;
		node = new Node;
		node->setCoef(data[i]);
		node->setExp(n - i - 1);
		m_data->prepend(node);
	}
}

Number::Number(char data[], int n, SIGN sign)
	:m_sign(sign)
{
	m_data = new LinkedList;
	if (!n)
		return;
	Node* node = 0;


	// get the index of '.'
	// index==n means this number is an integer
	int index = n;
	for (int i = 0; i < n; i++)
	{
		if (data[i] == '.')
		{
			index = i;
			break;
		}
			
	}

	// 0->index-1 is the integral part
	for (int i = 0; i < index; i++)
	{
		int num = int(data[i])-int('0');
		if (!num)
			continue;
		node = new Node;
		node->setCoef(num);
		node->setExp(index - i - 1);
		m_data->prepend(node);
	}

	// index+1->n is the decimal part
	for (int i = index + 1; i < n; i++)
	{
		int num = int(data[i]) - int('0');
		if (!num)
			continue;
		node = new Node;
		node->setCoef(num);
		node->setExp(index - i);
		m_data->prepend(node);
	}
}

Number::Number() 
:m_sign(POSITIVE)
{
	m_data = new LinkedList;
}

Number::Number(const Number& num)
{
	m_sign = num.m_sign;
	m_data = num.m_data->clone();
}



Number::~Number()
{
	if (m_data)
	{
		delete m_data;
	}
}

Number& Number::operator=(const Number& num)
{
	m_sign = num.m_sign;
	m_data = num.m_data->clone();
	return (*this);
}

Number Number::operator+(const Number& num)
{
	Number sum;
	if (this->m_sign*num.m_sign > 0)
	{
		if (this->m_sign>0)
		{
			sum.m_sign = POSITIVE;
		}
		else
		{
			sum.m_sign = NEGATIVE;
		}

		Node* nodeA = this->m_data->nodeAt(0);
		Node* nodeB = num.m_data->nodeAt(0);
		Node* newNode = 0;
		while (nodeA && nodeB)
		{
			if (nodeA->exp() == nodeB->exp())
			{
				newNode = new Node(nodeA->coef() + nodeB->coef(), nodeA->exp());
				sum.m_data->append(newNode);
				nodeA = nodeA->next();
				nodeB = nodeB->next();
			}
			else if (nodeA->exp() < nodeB->exp())
			{
				newNode = new Node(nodeA->coef(), nodeA->exp());
				sum.m_data->append(newNode);
				nodeA = nodeA->next();
			}
			else
			{
				newNode = new Node(nodeB->coef(), nodeB->exp());
				sum.m_data->append(newNode);
				nodeB = nodeB->next();
			}

		}
		while (nodeA)
		{
			newNode = new Node(nodeA->coef(), nodeA->exp());
			sum.m_data->append(newNode);
			nodeA = nodeA->next();
		}
		while (nodeB)
		{
			newNode = new Node(nodeB->coef(), nodeB->exp());
			sum.m_data->append(newNode);
			nodeB = nodeB->next();
		}

		Node* node = sum.m_data->nodeAt(0);
		int index = 0;
		while (node)
		{
			if (node->coef() > 9)
			{
				if (node->next() && node->next()->exp() == node->exp() + 1)
				{
					node->next()->m_coef += node->coef() / 10;
				}
				else
				{
					newNode = new Node(node->coef() / 10, node->exp() + 1);
					newNode->setNext(node->next());
					node->setNext(newNode);
				}
				node->m_coef %= 10;
			}
			else if (!node->coef())
			{
				Node* pNextNode = node->next();
				sum.m_data->removeAt(index);
				node = pNextNode;
				index++;
				continue;
			}

			node = node->next();
			index++;
		}
            
	}
	else
	{
		if ((*this) == num)
			return sum;
		else 
		{

		}
	}

	return sum;
}

Number Number::operator*(const Number& num)
{
	Number multiply;
	if (this->m_sign != num.m_sign)
	{
		multiply.m_sign = NEGATIVE;
	}
	if (this->m_data->isEmpty() || num.m_data->isEmpty())
		return multiply;

	Node* pNode = num.m_data->nodeAt(0);
	while (pNode)
	{
		Number mult = *this;
		Node* node = mult.m_data->nodeAt(0);
		while (node)
		{
			node->m_coef *= pNode->coef();
			node->m_exp += pNode->exp();
			node = node->next();
		}
		multiply += mult;

		pNode = pNode->next();
	}

	return multiply;
}

Number& Number::operator+=(const Number& num)
{
	(*this) = (*this) + num;
	return (*this);
}

Number Number::operator-(const Number& num)
{
	Number temp = num;
	temp.m_sign = SIGN(-temp.m_sign);
	return (*this + temp);
}

bool Number::operator==(const Number& num)
{
	if (this->m_sign != num.m_sign || this->m_data->length() != num.m_data->length())
	{
		return false;
	}
	Node* nodeA = this->m_data->nodeAt(0);
	Node* nodeB = this->m_data->nodeAt(0);
	while (nodeA && nodeB)
	{
		if (nodeA->coef()!=nodeB->coef() || nodeA->exp()!=nodeB->exp())
		{
			return false;
		}
		else
		{
			nodeA = nodeA->next();
			nodeB = nodeB->next();
		}
	}
	return true;
}

bool Number::operator<(const Number& num)
{
	if (this->m_sign == num.m_sign)
	{
		int lenA = this->m_data->length();
		int lenB = num.m_data->length();
		while (lenA && lenB)
		{
			Node* nodeA = this->m_data->nodeAt(lenA-1);
			Node* nodeB = num.m_data->nodeAt(lenB-1);
			if (nodeA->exp() < nodeB->exp())
			{
				return false;
			}
			else if (nodeA->exp() > nodeB->exp())
			{
				return true;
			}
			else
			{
				if (nodeA->coef() < nodeB->coef())
				{
					return false;
				}else if (nodeA->coef() > nodeB->coef())
				{
					return true;
				}
				else
				{
					lenA--;
					lenB--;
				}
			}
		}
		if (lenA)
		{
			return true;
		}
		
		return false;
	}
	else if (this->m_sign == NEGATIVE)
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool Number::operator>(const Number& num)
{
	if ((*this) == num)
		return false;
	return !((*this) < num);
}

ostream& operator<<(ostream& os, const Number& num)
{
	if (num.m_data->isEmpty())
	{
		os << '0';
		return os;
	}
	os << ((num.m_sign > 0) ? "" : "-");
	int len = num.m_data->length();
	Node* node = num.m_data->nodeAt(0);
	int index = 0;
	while (node)
	{
		if (node->exp()>=0)
		{
			break; 
		}
		node = node->next();
		index++;
	}

	if (index == len)
	{
		// without integral part
		os << "0.";
	}
	else
	{
		// cout integral part
		for (int i = len-1; i >= index;i--)
		{
			Node* currNode = num.m_data->nodeAt(i);
			os << currNode->coef();
			int zeroNum = 0;
			if (i == index)
			{
				zeroNum = currNode->exp();
			}
			else
			{
				zeroNum = currNode->exp() - num.m_data->nodeAt(i - 1)->exp() - 1;
			}
			for (int j = 0; j < zeroNum; j++)
			{
				cout << '0';
			}
		}
		if (index > 0)
			os << '.';
	}

	// cout decimal part
	for (int i = index - 1; i >= 0;i--)
	{
		Node* currNode = num.m_data->nodeAt(i);
		int zeroNum = 0;
		if (i == index-1)
		{
			zeroNum = -1 * (1 + currNode->exp());
		}
		else
		{
			zeroNum = currNode->next()->exp() - currNode->exp() - 1;
		}
		for (int j = 0; j < zeroNum; j++)
		{
			cout << '0';
		}
		cout << currNode->coef();
	}

	return os;
}


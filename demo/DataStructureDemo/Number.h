#ifndef NUMBER_H
#define NUMBER_H

#include "LinkedList.h"

class Number
{
public:
	enum SIGN
	{
		POSITIVE = 1,
		NEGATIVE = -1
	};

	Number();

	Number(const Number& num);

	~Number();

	Number(int data[], int n, SIGN sign = POSITIVE);

	Number(char data[], int n, SIGN sign = POSITIVE);

	friend ostream& operator<<(ostream& os, const Number& num);
	Number operator+(const Number& num);
	Number operator-(const Number& num);
	bool operator==(const Number& num);
	bool operator<(const Number& num);
	bool operator>(const Number& num);
	Number& operator+=(const Number& num);
	Number operator*(const Number& num);
	Number& operator=(const Number& num);

private:
	LinkedList* m_data;
	SIGN m_sign;

};





#endif
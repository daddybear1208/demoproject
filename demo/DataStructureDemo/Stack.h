#ifndef STACK_H
#define STACK_H

//  Stack.h
//  This is header file of Stack
//  Created by GEEK on 4/8/15.
//  Copyright (c) 2015 BEAR. All rights reserved.
//

//	Note:
//	1. StackElement struct can be customized
//	2. Use an array of StackElement for underlying data structure, and dynamically allocate it's memory
//	3. You can add content to PRIVATE section of the class, but DO NOT change the public interface

struct StackElement 
{
	int data;
};

class Stack
{
public:
	//! construct an empty stack
	Stack();
	//! destructor
	~Stack();
	//! copy constructor
	Stack(const Stack& stack);

	//! return true if the stack is empty
	bool empty() const;

	//! return the number of elements
	int size() const;

	//! return a pointer of top element
	StackElement& top() const;

	//! remove the top element
	void pop();

	//! insert element at the top
	void push(const StackElement& element);

	// assignment operator
	Stack& operator=(const Stack& num);

private:
	// use array for underlying data structure
	StackElement* data_;
};


#endif
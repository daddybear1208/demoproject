//  BigNumber
//
//  Created by Yunlei Liang on 3/30/15.
//  Copyright (c) 2015 Mushroom. All rights reserved.
//

#include <iostream>
using namespace std;

#include "Number.h"
int main(int argc, char* argv[])
{

	// read file
	FILE* fi = fopen("E:/Projects/DemoProject/src/demo/DataStructureDemo/num0.txt", "r");
	int count = 0;
	char c;
	while ((c = fgetc(fi)) != EOF) count++;
	fseek(fi, 0, SEEK_SET);
	char* data0 = new char[count];
	for (int i = 0; i < count;i++)
	{
		*(data0 + i) = fgetc(fi);
	}

	Number num0(data0, count);


	fclose(fi); count = 0;
	fi = fopen("E:/Projects/DemoProject/src/demo/DataStructureDemo/num1.txt", "r");
	while ((c = fgetc(fi)) != EOF) count++;
	fseek(fi, 0, SEEK_SET);
	char* data1 = new char[count];
	for (int i = 0; i < count; i++)
	{
		*(data1 + i) = fgetc(fi);
	}
	fclose(fi);
	Number num1(data1, count);

	cout << num0 << " + " << num1 << " = " << num0 + num1 << endl;
	cout << num0 << " * " << num1 << " = " << num0 * num1 << endl;

	delete[] data0;
	delete[] data1;
	system("pause");
	return 0;
}









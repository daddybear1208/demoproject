#include <iostream>
using namespace std;

#include <pcl/io/ascii_io.h>
#include <pcl/octree/octree.h>
#include <pcl/visualization/cloud_viewer.h>

int main(int argc, char* argv[])
{
	pcl::PCLPointCloud2 pointCloud;
	pcl::ASCIIReader reader;
	reader.read("E:/GraduateProject/Data/city1.txt", pointCloud);
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloudPtr(new pcl::PointCloud<pcl::PointXYZ>);
	pcl::fromPCLPointCloud2(pointCloud, *cloudPtr);
	cout << cloudPtr->width;
	return 0;
	

}
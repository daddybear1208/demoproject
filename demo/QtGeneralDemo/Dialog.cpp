#include "Dialog.h"
#include <QFile>
#include <QVBoxLayout>
#include <QSettings>

Dialog::Dialog(QWidget *parent /*= 0*/)
{
	createGUI();
	reload();
}

Dialog::~Dialog(void)
{
	
}

void Dialog::reload(void)
{
	restoreState();
}

void Dialog::createGUI(void)
{
	QFile file(":/Resources/default.txt");
	file.open(QIODevice::ReadOnly);
	model = new TreeModel(file.readAll());
	file.close();

	view = new QTreeView(this);
	view->setModel(model);

	QVBoxLayout *mainVLayout = new QVBoxLayout;
	mainVLayout->addWidget(view);

	setLayout(mainVLayout);
}

void Dialog::closeEvent(QCloseEvent *)
{
	saveState();
}

void Dialog::saveState(void)
{
	QStringList List;

	// prepare list
	// PS: getPersistentIndexList() function is a simple `return this->persistentIndexList()` from TreeModel model class
	foreach(QModelIndex index, model->getPersistentIndexList())
	{
		if (view->isExpanded(index))
		{
			List << index.data(Qt::DisplayRole).toString();
		}
	}

	// save list
	QSettings settings("settings.ini", QSettings::IniFormat);
	settings.beginGroup("MainWindow");
	settings.setValue("ExpandedItems", QVariant::fromValue(List));
	settings.endGroup();
}

void Dialog::restoreState(void)
{
	QStringList List;

	// get list
	QSettings settings("settings.ini", QSettings::IniFormat);
	settings.beginGroup("MainWindow");
	List = settings.value("ExpandedItems").toStringList();
	settings.endGroup();

	foreach(QString item, List)
	{
		// search `item` text in model
		QModelIndexList Items = model->match(model->index(0, 0), Qt::DisplayRole, QVariant::fromValue(item));
		if (!Items.isEmpty())
		{
			// Information: with this code, expands ONLY first level in QTreeView
			view->setExpanded(Items.first(), true);
		}
	}
}


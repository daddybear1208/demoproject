#ifndef DIALOG_H
#define DIALOG_H
#include <QDialog>
#include <QTreeView>

#include "treemodel.h"

class Dialog : public QDialog
{
	Q_OBJECT;

	TreeModel *model;
	QTreeView *view;

public:
	Dialog(QWidget *parent = 0);
	~Dialog(void);

	void reload(void);

protected:
	void createGUI(void);
	void closeEvent(QCloseEvent *);
	void saveState(void);
	void restoreState(void);
};


#endif
#ifndef FRAMETRANSFORM_H
#define FRAMETRANSFORM_H

#include <QGLViewer/qglviewer.h>
class Viewer :public QGLViewer
{
protected:
	virtual void init();
	virtual void draw();
	virtual QString helpString() const;
};

#endif // !MANIPULATEDFRAME_H


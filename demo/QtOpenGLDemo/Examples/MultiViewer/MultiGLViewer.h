#ifndef MULTIGLVIEWER_H
#define MULTIGLVIEWER_H
#include <QGLViewer/qglviewer.h>


class Scene
{
public:
	void draw() const;
};

class MultiGLViewer :public QGLViewer
{
public:
	explicit MultiGLViewer(const Scene* const s, int type, QWidget* parent, const QGLWidget* shareWidget = NULL);;

	~MultiGLViewer();

protected:
	virtual void draw();

private:
	const Scene* const scene_;
};

#endif
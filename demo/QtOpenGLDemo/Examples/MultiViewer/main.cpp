#include "MultiGLViewer.h"
#include <qapplication.h>
#include <qsplitter.h>

int main(int argc, char** argv)
{
	QApplication application(argc, argv);

	// Create Splitters
	QSplitter *hSplit = new QSplitter(Qt::Vertical);
	QSplitter *vSplit1 = new QSplitter(hSplit);
	QSplitter *vSplit2 = new QSplitter(hSplit);

	// Create the scene
	Scene* s = new Scene();

	// Instantiate the viewers.
	MultiGLViewer side(s, 0, vSplit1);
	MultiGLViewer top(s, 1, vSplit1, &side);
	MultiGLViewer front(s, 2, vSplit2, &side);
	MultiGLViewer persp(s, 3, vSplit2, &side);


	hSplit->setWindowTitle("multiView");

	// Set main QSplitter as the main widget.
	hSplit->show();

	return application.exec();
}
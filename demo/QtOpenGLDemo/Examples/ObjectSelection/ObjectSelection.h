#ifndef OBJECTSELECTION_H
#define OBJECTSELECTION_H

#include <QGLViewer/qglviewer.h>
#include <QGLViewer/camera.h>
#include <QGLViewer/manipulatedCameraFrame.h>

class Viewer : public QGLViewer
{
protected:
	virtual void draw();
	virtual void drawWithNames();
	virtual void postSelection(const QPoint& point);
	virtual void init();
	virtual QString helpString() const;

private:
	qglviewer::Vec orig, dir, selectedPoint;
};

#endif // !OBJECTSELECTION_H


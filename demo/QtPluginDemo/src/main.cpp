#include "mainwindow.h"
#include <QtPlugin>
#include <QApplication>

QT_BEGIN_NAMESPACE
Q_IMPORT_PLUGIN(plugin_basic_tools)
QT_END_NAMESPACE

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    MainWindow window;
    window.show();
    return app.exec();
}
//! [0]

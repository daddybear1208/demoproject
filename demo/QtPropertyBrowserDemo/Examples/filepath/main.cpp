#include <qtpropertybrowser.h>
#include <qtpropertymanager.h>

class FilePathManager : public QtAbstractPropertyManager
{
	Q_OBJECT

protected:
	virtual bool hasValue(const QtProperty *property) const;
	virtual QIcon valueIcon(const QtProperty *property) const;
	virtual QString valueText(const QtProperty *property) const;
	virtual void initializeProperty(QtProperty *property);
	virtual void uninitializeProperty(QtProperty *property);
	virtual QtProperty *createProperty();

};


int main(int argc, char* argv)
{

}
#include <iostream>
#include <sys/locking.h>
using namespace std;

#include <QObject>
QT_BEGIN_NAMESPACE
#include <QtXml>
#include <QDomDocument>
#include <QDateTime>
#include <QApplication>
QT_END_NAMESPACE


int main(int argc, char* argv)
{
	// write xml document

	QDomDocument doc;
	QDomProcessingInstruction xmlDecl = doc.createProcessingInstruction("xml", "version='1.0' encoding='utf-8'");
	doc.appendChild(xmlDecl);
	QDomElement rootElement = doc.createElement("CloudStudioProject");
	rootElement.setAttribute("Creator", "Hongxiong Li");
	rootElement.setAttribute("Time", QDateTime::currentDateTimeUtc().toString("MM/dd/yyyy hh:mm:ss"));
	rootElement.setAttribute("Version", "0.1.0");
	doc.appendChild(rootElement);
	QFile file("E:/GraduateProject/Data/test.xml");
	if (file.open(QIODevice::WriteOnly))
	{
		QTextStream fileStream(&file);
		doc.save(fileStream, 4);
		file.close();
	}
	

	// read xml document

// 	QDomDocument doc;
// 	QFile file("E:/GraduateProject/Data/test.xml");
// 	if (file.open(QIODevice::ReadOnly))
// 	{
// 		doc.setContent(&file);
// 		file.close();
// 		QDomElement rootElement = doc.documentElement();
// 		cout << qPrintable(rootElement.tagName()) << endl;
// 		QDomElement generalElement = rootElement.firstChildElement("GeneralInfo");
// 		if (!generalElement.isNull())
// 		{
// 			cout << "	GeneralInfo\n";
// 		}
// 
// 		QDomElement element = generalElement.firstChildElement("Creator");
// 		cout << "		Creator: " << qPrintable(element.firstChild().toText().data()) << endl;
// 		element = generalElement.firstChildElement("Time");
// 		cout << "		Time: " << qPrintable(element.firstChild().toText().data()) << endl;
// 	}






	system("pause");
	return 0;
}